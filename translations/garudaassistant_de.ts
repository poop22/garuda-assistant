<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>GarudaAssistant</name>
    <message>
        <location filename="../garudaassistant.ui" line="14"/>
        <source>Garuda-Assistant</source>
        <translation type="unfinished">Garuda Assistent</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="31"/>
        <source>Maintenance</source>
        <translation type="unfinished">Wartung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="37"/>
        <source>Reset Configs</source>
        <translation type="unfinished">Konfiguration zurücksetzen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="46"/>
        <source>Reset Selected Configs (This might overwrite your customizations)</source>
        <translation type="unfinished">Ausgewählte Konfigurationen zurücksetzen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="71"/>
        <source>System</source>
        <translation type="unfinished">System</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="77"/>
        <source>Clear package cache</source>
        <translation type="unfinished">Zwischengespeicherte Pakete löschen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="97"/>
        <source>System update</source>
        <translation type="unfinished">System aktualisieren</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="117"/>
        <source>Edit repositories</source>
        <translation type="unfinished">Repositories bearbeiten</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="137"/>
        <source>Remove orphans</source>
        <translation type="unfinished">Verwaiste Pakete entfernen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="157"/>
        <source>Clear caches</source>
        <translation type="unfinished">Cache löschen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="177"/>
        <source>Refresh mirrorlist (Reflector-Simple)</source>
        <translation type="unfinished">Spiegelserverliste erneuern</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="197"/>
        <source>Remove database lock</source>
        <translation type="unfinished">Pacman Datenbank entsperren</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="217"/>
        <source>Refresh Keyrings</source>
        <translation type="unfinished">Keyrings erneuern</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="237"/>
        <source>Reinstall all packages</source>
        <translation type="unfinished">Alle Pakete neu installieren</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="265"/>
        <source>Btrfs</source>
        <translation type="unfinished">BTRFS</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="292"/>
        <source>Internal Filesystem Statistics</source>
        <translation type="unfinished">Dateisystem Statistiken</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="298"/>
        <source>Data:</source>
        <translation type="unfinished">Daten:</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="312"/>
        <source>Metadata:</source>
        <translation type="unfinished">Metadaten:</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="325"/>
        <source>Balance not currently needed</source>
        <translation type="unfinished">Balancing nicht erforderlich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="335"/>
        <source>Start Full Balance</source>
        <translation type="unfinished">Balancing starten</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="359"/>
        <source>System:</source>
        <translation type="unfinished">System:</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="375"/>
        <source>Information</source>
        <translation type="unfinished">Informationen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="381"/>
        <source>Used:</source>
        <translation type="unfinished">Benutzt:</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="388"/>
        <source>Allocated:  </source>
        <translation type="unfinished">Zugewiesen:  </translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="425"/>
        <source>Filesystem Size: </source>
        <translation type="unfinished">Dateisystemgröße: </translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="432"/>
        <source>Free(Estimated): </source>
        <translation type="unfinished">Frei (geschätzt): </translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="468"/>
        <source>Device Selection</source>
        <translation type="unfinished">Gerät auswählen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="474"/>
        <source>Device:</source>
        <translation type="unfinished">Gerät:</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="510"/>
        <source>Show subvolume tab</source>
        <translation type="unfinished">Subvolume Tab anzeigen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="513"/>
        <location filename="../garudaassistant.ui" line="2331"/>
        <source>none</source>
        <translation type="unfinished">keine</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="536"/>
        <source>Refresh BTRFS Data</source>
        <translation type="unfinished">BTRFS Daten laden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="552"/>
        <source>Timers</source>
        <translation type="unfinished">Timer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="558"/>
        <source>BTRFS scrub enabled</source>
        <translation type="unfinished">BTRFS scrub an</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="564"/>
        <location filename="../garudaassistant.ui" line="580"/>
        <location filename="../garudaassistant.ui" line="596"/>
        <location filename="../garudaassistant.ui" line="612"/>
        <location filename="../garudaassistant.ui" line="625"/>
        <location filename="../garudaassistant.ui" line="638"/>
        <location filename="../garudaassistant.ui" line="651"/>
        <location filename="../garudaassistant.ui" line="1332"/>
        <location filename="../garudaassistant.ui" line="1358"/>
        <location filename="../garudaassistant.ui" line="1371"/>
        <location filename="../garudaassistant.ui" line="1406"/>
        <location filename="../garudaassistant.ui" line="1432"/>
        <location filename="../garudaassistant.ui" line="1454"/>
        <location filename="../garudaassistant.ui" line="1493"/>
        <location filename="../garudaassistant.ui" line="1528"/>
        <location filename="../garudaassistant.ui" line="1541"/>
        <location filename="../garudaassistant.ui" line="1554"/>
        <location filename="../garudaassistant.ui" line="1602"/>
        <location filename="../garudaassistant.ui" line="1628"/>
        <location filename="../garudaassistant.ui" line="1711"/>
        <location filename="../garudaassistant.ui" line="1785"/>
        <location filename="../garudaassistant.ui" line="1798"/>
        <location filename="../garudaassistant.ui" line="1811"/>
        <location filename="../garudaassistant.ui" line="2075"/>
        <location filename="../garudaassistant.ui" line="2088"/>
        <location filename="../garudaassistant.ui" line="2101"/>
        <location filename="../garudaassistant.ui" line="2136"/>
        <location filename="../garudaassistant.ui" line="2228"/>
        <location filename="../garudaassistant.ui" line="2254"/>
        <location filename="../garudaassistant.ui" line="2280"/>
        <location filename="../garudaassistant.ui" line="2306"/>
        <source>service</source>
        <translation type="unfinished">service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="567"/>
        <source>btrfs-scrub.timer</source>
        <translation type="unfinished">btrfs-scrub.timer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="574"/>
        <source>BTRFS balance enabled</source>
        <translation type="unfinished">BTRFS Balancing an</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="583"/>
        <source>btrfs-balance.timer</source>
        <translation type="unfinished">btrfs-balance.timer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="590"/>
        <source>BTRFS trim enabled</source>
        <translation type="unfinished">BTRFS Trim an</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="599"/>
        <source>btrfs-trim.timer</source>
        <translation type="unfinished">btrfs-trim.timer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="606"/>
        <source>BTRFS defrag enabled</source>
        <translation type="unfinished">BTRFS defragmentieren ein</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="615"/>
        <source>btrfs-defrag.timer</source>
        <translation type="unfinished">btrfs-defrag.timer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="622"/>
        <source>Snapper timeline enabled</source>
        <translation type="unfinished">Snapper Timeline an</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="628"/>
        <source>snapper-timeline.timer</source>
        <translation type="unfinished">snapper-timerline.timer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="635"/>
        <source>Snapper cleanup enabled</source>
        <translation type="unfinished">Snapper aufräumen ein</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="641"/>
        <source>snapper-cleanup.timer</source>
        <translation type="unfinished">snapper-cleanup.timer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="648"/>
        <source>Snapper boot enabled</source>
        <translation type="unfinished">Snapper booten an</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="654"/>
        <source>snapper-boot.timer</source>
        <translation type="unfinished">snapper-boot.timer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="667"/>
        <location filename="../garudaassistant.ui" line="1963"/>
        <location filename="../garudaassistant.ui" line="2428"/>
        <source>Apply</source>
        <translation type="unfinished">Anwenden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="682"/>
        <source>Btrfs Subvolumes</source>
        <translation type="unfinished">BTRFS Subvolumes</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="688"/>
        <source>Include Timeshift and Snapper Snapshots</source>
        <translation type="unfinished">Timeshift und Snapper Snapshots anzeigen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="707"/>
        <source>Refresh Subvolumes</source>
        <translation type="unfinished">Subvolumes neu laden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="812"/>
        <source>Restore Snapshot</source>
        <translation type="unfinished">Snapshot wiederherstellen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="727"/>
        <source>Delete Selected</source>
        <translation type="unfinished">Ausgewählte löschen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="738"/>
        <location filename="../garudaassistant.cpp" line="1165"/>
        <source>Snapper</source>
        <translation>Snapper</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="750"/>
        <location filename="../garudaassistant.ui" line="893"/>
        <source>Select config: </source>
        <translation type="unfinished">Konfiguration auswählen: </translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="764"/>
        <source>Restore Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="784"/>
        <source>New Snapshot</source>
        <translation type="unfinished">Neuer Snapshot</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="798"/>
        <source>Delete Snapshot</source>
        <translation type="unfinished">Snapshot löschen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="839"/>
        <source>Show settings tab</source>
        <translation type="unfinished">Einstellungen anzeigen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="866"/>
        <source>Snapper - Settings</source>
        <translation type="unfinished">Snapper - Einstellungen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="916"/>
        <location filename="../garudaassistant.cpp" line="1212"/>
        <source>New Config</source>
        <translation type="unfinished">Neue Konfiguration</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="923"/>
        <source>Delete Config</source>
        <translation type="unfinished">Konfiguration löschen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="943"/>
        <source>Save Config</source>
        <translation type="unfinished">Konfiguration speichern</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="953"/>
        <location filename="../garudaassistant.ui" line="1016"/>
        <source>Config information</source>
        <translation type="unfinished">Konfiguration - Informationen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="959"/>
        <location filename="../garudaassistant.ui" line="1022"/>
        <source>Config name: </source>
        <translation type="unfinished">Name: </translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="986"/>
        <location filename="../garudaassistant.ui" line="1049"/>
        <source>Backup path: </source>
        <translation type="unfinished">Backup Pfad: </translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1085"/>
        <source>Snapshot retention</source>
        <translation type="unfinished">Snapshots behalten</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1091"/>
        <location filename="../garudaassistant.ui" line="1105"/>
        <location filename="../garudaassistant.ui" line="1152"/>
        <location filename="../garudaassistant.ui" line="1162"/>
        <location filename="../garudaassistant.ui" line="1169"/>
        <source>Save: </source>
        <translation type="unfinished">Speichern: </translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1118"/>
        <source>Hourly</source>
        <translation type="unfinished">Stündlich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1142"/>
        <source>Daily</source>
        <translation type="unfinished">Täglich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1098"/>
        <source>Weekly</source>
        <translation type="unfinished">Wöchentlich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1135"/>
        <source>Monthly</source>
        <translation type="unfinished">Monatlich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1125"/>
        <source>Yearly</source>
        <translation type="unfinished">Jährlich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1241"/>
        <source>System Components</source>
        <translation type="unfinished">Systemeinstellungen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1262"/>
        <source>Input method</source>
        <translation type="unfinished">Eingabemethode</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1268"/>
        <source>Fcitx</source>
        <translation type="unfinished">Fcitx</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1271"/>
        <location filename="../garudaassistant.ui" line="1284"/>
        <location filename="../garudaassistant.ui" line="1297"/>
        <location filename="../garudaassistant.ui" line="1310"/>
        <location filename="../garudaassistant.ui" line="1345"/>
        <location filename="../garudaassistant.ui" line="1419"/>
        <location filename="../garudaassistant.ui" line="1467"/>
        <location filename="../garudaassistant.ui" line="1480"/>
        <location filename="../garudaassistant.ui" line="1515"/>
        <location filename="../garudaassistant.ui" line="1576"/>
        <location filename="../garudaassistant.ui" line="1650"/>
        <location filename="../garudaassistant.ui" line="1698"/>
        <location filename="../garudaassistant.ui" line="1724"/>
        <location filename="../garudaassistant.ui" line="1824"/>
        <location filename="../garudaassistant.ui" line="1837"/>
        <location filename="../garudaassistant.ui" line="1885"/>
        <location filename="../garudaassistant.ui" line="1898"/>
        <location filename="../garudaassistant.ui" line="1911"/>
        <location filename="../garudaassistant.ui" line="1924"/>
        <location filename="../garudaassistant.ui" line="2001"/>
        <location filename="../garudaassistant.ui" line="2023"/>
        <location filename="../garudaassistant.ui" line="2036"/>
        <location filename="../garudaassistant.ui" line="2049"/>
        <location filename="../garudaassistant.ui" line="2062"/>
        <location filename="../garudaassistant.ui" line="2123"/>
        <location filename="../garudaassistant.ui" line="2202"/>
        <location filename="../garudaassistant.ui" line="2215"/>
        <location filename="../garudaassistant.ui" line="2241"/>
        <location filename="../garudaassistant.ui" line="2267"/>
        <location filename="../garudaassistant.ui" line="2293"/>
        <source>package</source>
        <translation type="unfinished">Packet</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1274"/>
        <source>fcitx-input-support</source>
        <translation type="unfinished">fcitx-input-support</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1281"/>
        <source>Ibus</source>
        <translation type="unfinished">Ibus</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1287"/>
        <source>ibus-input-support</source>
        <translation type="unfinished">ibus-input-support</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1294"/>
        <source>Fcitx5</source>
        <translation type="unfinished">Fcitx5</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1300"/>
        <source>fcitx5-input-support</source>
        <translation type="unfinished">fcitx5-input-support</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1307"/>
        <source>Asian fonts</source>
        <translation type="unfinished">Asiatische Fonts</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1313"/>
        <source>asian-fonts</source>
        <translation type="unfinished">asian-fonts</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1323"/>
        <source>Connman</source>
        <translation type="unfinished">Connman</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1335"/>
        <source>connman.service</source>
        <translation type="unfinished">connman.service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1342"/>
        <source>Connman support</source>
        <translation type="unfinished">Connman Unterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1348"/>
        <source>connman-support</source>
        <translation type="unfinished">Connman Unterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1361"/>
        <source>ofono.service</source>
        <translation type="unfinished">ofono.service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1374"/>
        <source>neard.service</source>
        <translation type="unfinished">neard.service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1384"/>
        <source>Samba</source>
        <translation type="unfinished">Samba</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1390"/>
        <source>User in sambashare group</source>
        <translation type="unfinished">Benutzer in sambashare Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1393"/>
        <location filename="../garudaassistant.ui" line="1589"/>
        <location filename="../garudaassistant.ui" line="1663"/>
        <location filename="../garudaassistant.ui" line="1685"/>
        <location filename="../garudaassistant.ui" line="1737"/>
        <location filename="../garudaassistant.ui" line="1750"/>
        <location filename="../garudaassistant.ui" line="1772"/>
        <location filename="../garudaassistant.ui" line="1850"/>
        <location filename="../garudaassistant.ui" line="1863"/>
        <location filename="../garudaassistant.ui" line="1937"/>
        <source>group</source>
        <translation type="unfinished">Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1396"/>
        <source>sambashare</source>
        <translation type="unfinished">sambashare</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1403"/>
        <source>SMB enabled</source>
        <translation type="unfinished">Samba benutzen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1409"/>
        <source>smb.service</source>
        <translation type="unfinished">smb.service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1416"/>
        <source>Samba support</source>
        <translation type="unfinished">Samba benutzen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1422"/>
        <source>samba-support</source>
        <translation type="unfinished">Samba Unterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1429"/>
        <source>NMB service</source>
        <translation type="unfinished">NMB Service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1435"/>
        <source>nmb.service</source>
        <translation type="unfinished">nmb.service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1445"/>
        <source>Firewall</source>
        <translation type="unfinished">Firewall</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1451"/>
        <source>ufw enabled</source>
        <translation type="unfinished">UFW benutzen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1457"/>
        <source>ufw.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1470"/>
        <source>ufw</source>
        <translation type="unfinished">UFW</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1483"/>
        <source>firewalld</source>
        <translation type="unfinished">Firewalld</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1490"/>
        <source>firewalld enabled</source>
        <translation type="unfinished">Firewalld benutzen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1496"/>
        <source>firewalld.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1506"/>
        <source>NetworkManager</source>
        <translation type="unfinished">Netzwerkmanager</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1512"/>
        <source>NetworkManager support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1518"/>
        <source>networkmanager-support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1525"/>
        <source>NetworkManager enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1531"/>
        <source>NetworkManager.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1538"/>
        <source>ModemManager enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1544"/>
        <source>ModemManager.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1551"/>
        <source>gpsd enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1557"/>
        <source>gpsd.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1567"/>
        <source>Bluetooth</source>
        <translation type="unfinished">Bluetooth</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1573"/>
        <source>Bluetooth support</source>
        <translation type="unfinished">Bluetooth Unterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1579"/>
        <source>bluetooth-support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1586"/>
        <source>User in lp group</source>
        <translation type="unfinished">Benutzer in lp Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1592"/>
        <source>lp</source>
        <translation type="unfinished">lp</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1599"/>
        <source>Bluetooth enabled</source>
        <translation type="unfinished">Bluetooth aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1605"/>
        <source>bluetooth.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1612"/>
        <source>Pulseaudio-bluetooth-autoconnect enabled</source>
        <translation type="unfinished">PA Bluetooth automatisch verbinden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1615"/>
        <source>global_service</source>
        <translation type="unfinished">Globaler Service</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1618"/>
        <source>pulseaudio-bluetooth-autoconnect.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1625"/>
        <source>Bluetooth-autoconnect enabled</source>
        <translation type="unfinished">Bluetooth automatisch verbinden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1631"/>
        <source>bluetooth-autoconnect.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1641"/>
        <source>Video</source>
        <translation type="unfinished">Video</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1647"/>
        <source>Gstreamer Codecs</source>
        <translation type="unfinished">GStreamer Codecs</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1653"/>
        <source>gstreamer-meta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1660"/>
        <source>User in video group</source>
        <translation type="unfinished">Benutzer in video Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1666"/>
        <source>video</source>
        <translation type="unfinished">video</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1676"/>
        <source>Virtualization</source>
        <translation type="unfinished">Virtualisierung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1682"/>
        <source>User in vboxusers group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1688"/>
        <source>vboxusers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1695"/>
        <source>Virtualbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1701"/>
        <source>virtualbox-meta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1708"/>
        <source>libvirtd enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1714"/>
        <source>libvirtd.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1721"/>
        <source>Virt-manager</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1727"/>
        <source>virt-manager-meta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1734"/>
        <source>User in libvirt group</source>
        <translation type="unfinished">Benutzer in libvirt Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1740"/>
        <source>libvirt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1747"/>
        <source>User in kvm group</source>
        <translation type="unfinished">Benutzer in kvm Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1753"/>
        <source>kvm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1763"/>
        <source>Printing and Scanning</source>
        <translation type="unfinished">Drucken und Scannen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1769"/>
        <source>User in cups group</source>
        <translation type="unfinished">Benutzer in cups Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1775"/>
        <source>cups</source>
        <translation type="unfinished">cups</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1782"/>
        <source>CUPS enabled</source>
        <translation type="unfinished">CUPS aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1788"/>
        <source>cups.socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1795"/>
        <source>Saned enabled</source>
        <translation type="unfinished">Saned eingeschaltet</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1801"/>
        <source>saned.socket</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1329"/>
        <source>Connman enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1189"/>
        <source>Pacman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1196"/>
        <source>Save:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1213"/>
        <source>Enable timeline snapshots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1355"/>
        <source>oFono enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1368"/>
        <source>Neard enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1808"/>
        <source>IPP USB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1814"/>
        <source>ipp-usb.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1821"/>
        <source>Printing support</source>
        <translation type="unfinished">Druckeruntersützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1827"/>
        <source>printer-support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1834"/>
        <source>Scanning support</source>
        <translation type="unfinished">Scannerunterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1840"/>
        <source>scanner-support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1847"/>
        <source>User in sys group</source>
        <translation type="unfinished">Benutzer in sys Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1853"/>
        <source>sys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1860"/>
        <source>User in scanner group</source>
        <translation type="unfinished">Benutzer in scanner Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1866"/>
        <source>scanner</source>
        <translation type="unfinished">Scanner</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1876"/>
        <source>Audio</source>
        <translation type="unfinished">Audio</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1882"/>
        <source>JACK support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1888"/>
        <source>jack-support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1895"/>
        <source>Pulseaudo support</source>
        <translation type="unfinished">PulseAudio Unterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1901"/>
        <source>pulseaudio-support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1908"/>
        <source>ALSA support</source>
        <translation type="unfinished">ALSA Unterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1914"/>
        <source>alsa-support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1921"/>
        <source>Pipewire support</source>
        <translation type="unfinished">PipeWire Unterstützung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1927"/>
        <source>pipewire-support</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1934"/>
        <source>User in realtime group</source>
        <translation type="unfinished">Benutzer in realtime Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1940"/>
        <source>realtime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1971"/>
        <source>Settings</source>
        <translation type="unfinished">Einstellungen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1992"/>
        <source>Guest user</source>
        <translation type="unfinished">Gastnutzer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="1998"/>
        <source>Guest user support</source>
        <translation type="unfinished">Gast in user Gruppe</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2004"/>
        <source>systemd-guest-user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2014"/>
        <source>Performance Tweaks</source>
        <translation type="unfinished">Performance Tweaks</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2026"/>
        <source>ananicy-cpp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2033"/>
        <source>Performance tweaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2039"/>
        <source>performance-tweaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2052"/>
        <source>irqbalance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2065"/>
        <source>uresourced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2072"/>
        <source>Ananicy Cpp enabled</source>
        <translation type="unfinished">Ananicy Cpp aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2078"/>
        <source>ananicy-cpp.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2085"/>
        <source>UResourced enabled</source>
        <translation type="unfinished">UResourced aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2091"/>
        <source>uresourced.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2098"/>
        <source>Irqbalance enabled</source>
        <translation type="unfinished">Irqbalance aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2104"/>
        <source>irqbalance.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2114"/>
        <source>Right click emulation</source>
        <translation type="unfinished">Rechtsklick Emulierung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2120"/>
        <source>Evdev long press right click emulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2126"/>
        <source>evdev-right-click-emulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2133"/>
        <source>Evdev-rce enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2139"/>
        <source>evdev-rce.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2149"/>
        <source>Hblock</source>
        <translation type="unfinished">HBlock</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2155"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;An adblocker that creates a hosts file from automatically downloaded sources&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2158"/>
        <source>Hblock enabled</source>
        <translation type="unfinished">Hblock aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2161"/>
        <location filename="../garudaassistant.ui" line="2405"/>
        <source>custom</source>
        <translation type="unfinished">Benutzerdefiniert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2164"/>
        <source>hblock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2174"/>
        <source>Dns</source>
        <translation type="unfinished">DNS</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2180"/>
        <source>Choose DNS service</source>
        <translation type="unfinished">Einen DNS server auswählen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2193"/>
        <source>Powersave Tweaks</source>
        <translation type="unfinished">Energiespar Tweaks</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2205"/>
        <source>thermald</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2218"/>
        <source>intel-undervolt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2225"/>
        <source>Intel-undervolt enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2231"/>
        <source>intel-undervolt.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2238"/>
        <source>Powersave tweaks</source>
        <translation type="unfinished">Energiespar Tweaks</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2244"/>
        <source>powersave-tweaks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2251"/>
        <source>Thermald enabled</source>
        <translation type="unfinished">Thermald eingeschaltet</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2257"/>
        <source>thermald.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2270"/>
        <source>tlp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2277"/>
        <source>TLP enabled</source>
        <translation type="unfinished">TLP aktiviert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2283"/>
        <source>tlp.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2296"/>
        <source>auto-cpufreq</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2303"/>
        <source>Auto-cpufreq enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2309"/>
        <source>auto-cpufreq.service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2319"/>
        <source>Switch Shells</source>
        <translation type="unfinished">Shell ändern</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2325"/>
        <source>Install default shell configs</source>
        <translation type="unfinished">Standartkonfiguration übernehmen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2373"/>
        <source>Default Shell:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2396"/>
        <source>GDM Wayland (GNOME only)</source>
        <translation type="unfinished">GDM Wayland (Nur GNOME)</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2402"/>
        <source>Enable GDM Wayland</source>
        <translation type="unfinished">Wayland in GDM einschalten</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2408"/>
        <source>gdm-wayland</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2436"/>
        <source>System Specs</source>
        <translation type="unfinished">Systeminfos</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2474"/>
        <source>Refreshing.....</source>
        <translation type="unfinished">Neu laden.....</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2493"/>
        <location filename="../garudaassistant.ui" line="2632"/>
        <source>Copy for Forum</source>
        <translation type="unfinished">Für das Forum kopieren</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2512"/>
        <location filename="../garudaassistant.ui" line="2613"/>
        <source>Open Forum</source>
        <translation type="unfinished">Forum öffnen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2536"/>
        <source>Refresh</source>
        <translation type="unfinished">Neu laden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2561"/>
        <source>Other Diagnostics</source>
        <translation type="unfinished">Andere Diagnosedaten</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2575"/>
        <source>Select a button below to load diagnostic information</source>
        <translation type="unfinished">Einen der unteren Buttons zum Daten laden drücken</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2594"/>
        <source>Systemd Analyze</source>
        <translation type="unfinished">Systemanalyse</translation>
    </message>
    <message>
        <location filename="../garudaassistant.ui" line="2651"/>
        <source>Journal Errors</source>
        <translation type="unfinished">Journal Fehlermeldung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="160"/>
        <location filename="../garudaassistant.cpp" line="628"/>
        <source>Garuda Assistant</source>
        <translation type="unfinished">Garuda Assistent</translation>
    </message>
    <message>
        <source>Elevated rights required for application function</source>
        <oldsource>Elevated rights required for application function

Exiting....</oldsource>
        <translation type="obsolete">Erweiterte Rechte zum Ausführen der Anwendung erforderlich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="170"/>
        <source>Exiting....</source>
        <translation type="unfinished">Anwendung wird beendet....</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="170"/>
        <source>Authentication is required to run Garuda Assistant</source>
        <translation>Erweiterte Rechte zum Ausführen der Anwendung erforderlich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="300"/>
        <source>Failed to load groups for current user</source>
        <translation type="unfinished">Das Laden der Gruppen des Nutzers ist fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="432"/>
        <location filename="../garudaassistant.cpp" line="455"/>
        <source>Garuda Asssistant</source>
        <translation type="unfinished">Garuda Assistent</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="432"/>
        <source>Pacman database lock removed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="455"/>
        <source>Configs applied, please relogin to finish!</source>
        <translation type="unfinished">Konfiguration gespeichert, zum anwenden bitte neu anmelden!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="533"/>
        <source>Balance recommended.  Click here ----&gt;</source>
        <translation type="unfinished">Balancing empfohlen. Hier klicken ----&gt;</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="535"/>
        <source>Balance not needed at this time</source>
        <translation type="unfinished">Balancing ist gerade nicht erforderlich</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="545"/>
        <source>You have lots of free space, did you overbuy?</source>
        <translation type="unfinished">Du hast eine Menge freien Speicher, hast du zuviel gekauft?</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="547"/>
        <source>Situation critical!  Time to delete some data or buy more disk</source>
        <translation type="unfinished">Situation kritisch! Zeit Daten zu löschen oder mehr Speicher zu kaufen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="549"/>
        <source>Your disk space is well utilized</source>
        <translation type="unfinished">Dein Datenspeicher ist gut genutzt</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="563"/>
        <source>No device selected</source>
        <oldsource>No device selected
Please Select a device first</oldsource>
        <translation type="unfinished">Keine Gerät ausgewählt
Bitte erst ein Gerät auswählen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="563"/>
        <source>Please Select a device first</source>
        <translation type="unfinished">Bitte erst ein Gerät auswählen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="628"/>
        <source>Changes applied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="640"/>
        <source>Nothing to delete!</source>
        <translation type="unfinished">Nichts zum löschen!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="648"/>
        <source>Failed to delete subvolume!</source>
        <oldsource>Failed to delete subvolume!

subvolid mising from map</oldsource>
        <translation type="unfinished">Subvolume löschen fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="648"/>
        <source>subvolid missing from map</source>
        <translation type="unfinished">Subvolid ist nicht auffindbar</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="655"/>
        <source>You cannot delete a mounted subvolume</source>
        <oldsource>You cannot delete a mounted subvolume

Please unmount the subvolume before continuing</oldsource>
        <translation type="unfinished">Du kannst kein eingehängtes Subvolume löschen

Bitte vor dem fortfahren aushängen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="655"/>
        <source>Please unmount the subvolume before continuing</source>
        <translation type="unfinished">Bitte das Subvolume erst aushängen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="666"/>
        <location filename="../garudaassistant.cpp" line="1255"/>
        <source>Please Confirm</source>
        <translation type="unfinished">Bitte bestätigen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="667"/>
        <source>You are about to delete all subvolumes associated with timeshift snapshot </source>
        <translation type="unfinished">Wirklich alle Subvolumes löschen, die mit folgendem Snapshot assoziert sind </translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="668"/>
        <source>Are you sure you want to proceed?</source>
        <oldsource>

Are you sure you want to proceed?</oldsource>
        <translation type="unfinished">Bist du sicher, dass du fortfahren möchtest?</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="673"/>
        <source>Snapshot Delete</source>
        <translation type="unfinished">Snapshot löschen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="674"/>
        <source>That subvolume is a snapper shapshot</source>
        <oldsource>That subvolume is a snapper shapshot

Please use the snapper tab to remove it</oldsource>
        <translation type="unfinished">Dieses Subvolume ist ein Snapper Snapshot

Bitte den Snapper Tab zum entfernen nutzen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="674"/>
        <source>Please use the snapper tab to remove it</source>
        <translation type="unfinished">Bitte den Snapper Tab zum löschen benutzen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="678"/>
        <location filename="../garudaassistant.cpp" line="789"/>
        <location filename="../garudaassistant.cpp" line="1055"/>
        <source>Confirm</source>
        <translation type="unfinished">Bestätigen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="678"/>
        <location filename="../garudaassistant.cpp" line="1256"/>
        <source>Are you sure you want to delete </source>
        <translation type="unfinished">Bist du sicher, dass du dies löschen möchtest </translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="692"/>
        <source>Process failed with output:</source>
        <oldsource>Process failed with output:

</oldsource>
        <translation type="unfinished">Prozess mit folgender Meldung fehlgeschlagen:</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="746"/>
        <source>Timeshift Snapshot</source>
        <translation type="unfinished">Timeshift Snapshot</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="746"/>
        <source>Please Timeshift to restore this snapshot</source>
        <translation type="unfinished">Bitte Timeshift öffnen um diesen Snapshot wiederherzustellen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="751"/>
        <source>This is not a snapshot that can be restored by this application</source>
        <translation type="unfinished">Dies ist kein Snapshot welcher von dieser Anwendung wiederhergestellt werden kann</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="758"/>
        <source>Failed to restore snapshot!</source>
        <oldsource>Failed to restore snapshot!

subvolid mising from map</oldsource>
        <translation type="unfinished">Wiederherstellung fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="768"/>
        <location filename="../garudaassistant.cpp" line="784"/>
        <source>Can&apos;t restore snapshot while the target subvolume is mounted</source>
        <translation type="unfinished">Kann keinen Snapshot wiederherstellen während das Ziel eingehängt ist</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="769"/>
        <source>Unmount the volume, reboot off a snapshot or use this tool from a live ISO to complete the restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="779"/>
        <source>Target not found</source>
        <translation type="unfinished">Ziel nicht gefunden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="790"/>
        <source>Are you sure you want to restore </source>
        <translation type="unfinished">Sicher folgendes wiederherzustellen </translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="790"/>
        <source> to </source>
        <comment>as in from/to</comment>
        <translation type="unfinished"> nach </translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="810"/>
        <source>Failed to make a backup of target subvolume</source>
        <translation type="unfinished">Backup des Subvolumes fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="824"/>
        <source>Failed to restore subvolume!</source>
        <translation type="unfinished">Subvolume Wiederherstellung fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="824"/>
        <source>Please verify the status of your system before rebooting</source>
        <translation type="unfinished">Bitte erst den Status des Systems vor den Neustart überprüfen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="833"/>
        <source>The restore was successful but the migration of the snapshots failed</source>
        <translation type="unfinished">Die Wiederherstellung war erfolgreich aber Migration der Snapshots ist fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="840"/>
        <source>Snapshot restoration complete.</source>
        <translation type="unfinished">Die Wiederherstellung ist abgeschlosssen.</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="840"/>
        <source>A copy of the original subvolume has been saved as </source>
        <translation type="unfinished">Eine Kopie das ursprünglichen Subvolumes wurde wie folgt gesichert </translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="965"/>
        <source>Subvolume</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1055"/>
        <source>Are you sure you want to delete the selected snapshot(s)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1234"/>
        <source>Cancel New Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1290"/>
        <source>Select Subvolume:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1296"/>
        <source>Select Config:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1305"/>
        <source>Please enter restore mode before trying to restore a snapshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1042"/>
        <location filename="../garudaassistant.cpp" line="1310"/>
        <source>Nothing selected!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1319"/>
        <source>Failed to restore snapshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1356"/>
        <source>Snapshot boot detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1357"/>
        <source>You are currently booted into snapshot </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1358"/>
        <source>Would you like to restore it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="987"/>
        <source>Number</source>
        <comment>The number associated with a snapshot</comment>
        <translation type="unfinished">Nummer</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1256"/>
        <source>This action cannot be undone</source>
        <translation type="unfinished">Diese Aktion kann nicht rückgängig gemacht werden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1518"/>
        <source>hblock settings changed, please reboot to apply</source>
        <translation type="unfinished">Hblock Einstellungen geändert, bitte neustarten zum anwenden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="834"/>
        <source>Please migrate the .snapshots subvolume manually</source>
        <oldsource>The restore was successful but the migration of the snapshots failed

Please migrate the .snapshots subvolume manually</oldsource>
        <translation type="unfinished">Die Wiederherstellung war erfolgreich aber die Migration der Snapshots ist fehlgeschlagen

Bitte manuell fortfahren</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="127"/>
        <source>Failed to find an installed terminal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="839"/>
        <source>Snapshot Restore</source>
        <translation type="unfinished">Snapshot Wiederherstellung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="841"/>
        <source>Please verify before rebooting</source>
        <oldsource>

Please verify before rebooting</oldsource>
        <translation type="unfinished">Bitte vor dem Neustart überprüfen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="966"/>
        <location filename="../garudaassistant.cpp" line="988"/>
        <source>Date/Time</source>
        <translation type="unfinished">Datum/Zeit</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="967"/>
        <location filename="../garudaassistant.cpp" line="989"/>
        <source>Description</source>
        <translation type="unfinished">Beschreibung</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1026"/>
        <source>No config selected for snapshot</source>
        <translation type="unfinished">Keine Konfiguration für Snapshot ausgewählt</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1063"/>
        <source>Cannot delete snapshot</source>
        <translation type="unfinished">Dieser Snapshot kann nicht gelöscht werden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1149"/>
        <source>Failed to save changes</source>
        <translation type="unfinished">Änderungen speichern fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1165"/>
        <source>Changes saved</source>
        <translation type="unfinished">Änderungen übernommen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1173"/>
        <source>Please enter a valid name</source>
        <translation type="unfinished">Bitte einen gültigen Namen eingeben</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1179"/>
        <source>That name is already in use!</source>
        <translation type="unfinished">Dieser Name existiert bereits!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1219"/>
        <source>No btrfs subvolumes found</source>
        <translation type="unfinished">Keine Subvolumes gefunden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1243"/>
        <source>No config selected</source>
        <translation type="unfinished">Keine Konfiguration ausgewählt</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1249"/>
        <source>You may not don&apos;t delete the root config</source>
        <translation type="unfinished">Diese Konfiguration kann nicht gelöscht werden</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1537"/>
        <source>Groups were added please logout so changes will take effect</source>
        <oldsource>Groups were added please logout so changes will take effect
</oldsource>
        <translation type="unfinished">Gruppen wurden hinzugefügt, zum anwenden bitte reloggen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1542"/>
        <source>Groups were removed please logout so changes will take effect</source>
        <oldsource>Groups were removed please logout so changes will take effect
</oldsource>
        <translation type="unfinished">Gruppen wurden entfernt, zum anwenden bitte reloggen</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1546"/>
        <source>Service state updated</source>
        <translation type="unfinished">Service Status aktualisiert</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1562"/>
        <source>DNS Updated!</source>
        <translation type="unfinished">DNS geupdated!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1593"/>
        <source>Shell changed, please relogin to apply changes!</source>
        <oldsource>Shell changed, please relogin to apply changes!
</oldsource>
        <translation type="unfinished">Shell verändert, zum anwenden bitte neu einloggen!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.cpp" line="1608"/>
        <source>Important!!</source>
        <translation type="unfinished">Wichtig!!</translation>
    </message>
    <message>
        <location filename="../garudaassistant.h" line="82"/>
        <source>All</source>
        <translation type="unfinished">Alles</translation>
    </message>
</context>
</TS>
