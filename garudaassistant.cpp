#include "garudaassistant.h"
#include "config.h"
#include "ui_garudaassistant.h"

GarudaAssistant::GarudaAssistant(QWidget *parent) : QMainWindow(parent), ui(new Ui::GarudaAssistant) { ui->setupUi(this); }

GarudaAssistant::~GarudaAssistant() {
    delete ui;
    runCmd("exit", false, true, 5000);
    runCmd("exit", false, false, 5000);
    rootBash.waitForFinished(5000);
    bash.waitForFinished(5000);
}

// Util function for getting bash command output and error code
Result GarudaAssistant::runCmd(QString cmd, bool includeStderr, bool escalate, int timeout) {
    QByteArray testString = "gar7864373";
    QByteArray command = cmd.toUtf8();
    QProcess *proc;

    if (escalate)
        proc = &rootBash;
    else
        proc = &bash;

    if (includeStderr)
        proc->setProcessChannelMode(QProcess::MergedChannels);

    command += " ; echo " + testString + "\n";

    if (!proc->waitForStarted(1000))
        return {1, ""};

    proc->write(command);

    QString output = "";
    int startTime = time(NULL);
    do {
        if (proc->waitForReadyRead(timeout == 0 ? -1 : 1000 * timeout)) {
            output += proc->readAll();
        }
        if (output.right(testString.length() + 1).trimmed() == testString)
            break;

    } while ((time(NULL) - startTime < 60 || !timeout) && proc->state() == QProcess::ProcessState::Running);

    if ((time(NULL) - startTime >= 60 && timeout) || proc->state() == QProcess::ProcessState::NotRunning)
        return {1, output.trimmed()};

    output = output.left(output.length() - (testString.length() + 1));

    return {0, output.trimmed()};
}

// Util function for getting bash command output and error code
// This version takes a list so multiple commands can be executed at once
Result GarudaAssistant::runCmd(QStringList cmdList, bool includeStderr, bool escalate, int timeout) {
    QString fullCommand;
    for (auto command : cmdList) {
        if (fullCommand == "")
            fullCommand = command;
        else
            fullCommand += "; " + command;
    }

    // Run the composite command as a single command
    return runCmd(fullCommand, includeStderr, escalate, timeout);
}

// Runs a command in a terminal, escalate's using pkexec if escalate is true
int GarudaAssistant::runCmdTerminal(QString cmd, bool escalate) {
    QProcess proc;
    cmd += "; read -p 'Press enter to exit'";
    if (escalate)
        proc.start(this->terminal.binary, QStringList() << this->terminal.param << "pkexec"
                                                        << "/bin/bash"
                                                        << "-c" << cmd);
    else
        proc.start(this->terminal.binary, QStringList() << this->terminal.param << "/bin/bash"
                                                        << "-c" << cmd);
    proc.waitForFinished(-1);
    return proc.exitCode();
}

// Convenience function to run a list of commands at once
int GarudaAssistant::runCmdTerminal(QStringList cmdList, bool escalate) {
    QString fullCommand;
    for (auto command : cmdList) {
        if (fullCommand == "")
            fullCommand = command;
        else
            fullCommand += " ; " + command;
    }

    // Run the composite command as a single command
    return runCmdTerminal(fullCommand, escalate);
}

// Kickoff an async refresh of the inxiData
void GarudaAssistant::refreshInxi() { inxiProc.start("/usr/bin/inxi", QStringList() << "-Fazc0"); }

// Process the inxi data and populate the QTextEdit
void GarudaAssistant::inxiReturn() {
    QString inxiOutput = inxiProc.readAllStandardOutput();

    const QStringList lines = inxiOutput.split("\n");
    for (const QString &line : lines) {
        if (!line.isEmpty()) {
            ui->textEdit_inxi->append(line);
        }
    }
}

// Sets the terminal to be used for commands which run a terminal
bool GarudaAssistant::setTerminal() {
    terminal = {};

    // Iterate over the list until we find an installed terminal
    for (Terminal term : terminalList) {
        if (QFile::exists(term.binary)) {
            terminal = term;
            break;
        }
    }

    if (terminal.binary.isEmpty()) {
        displayError(tr("Failed to find an installed terminal"));
        return false;
    }
    return true;
}

// Process the diagnostics data and populate the QTextEdit
void GarudaAssistant::diagnosticsReturn() {
    const QString diag = diagProc.readAllStandardOutput();
    ui->textEdit_diagnostics->append(diag);
}

// Adds checkboxes for any items present in /etc/skel to config reset
void GarudaAssistant::setupConfigBoxes() {
    QMapIterator<QString, ConfigItem> i(configActions);
    int row = 0;
    int col = 0;

    while (i.hasNext()) {
        i.next();
        if (QFile::exists(i.value().source)) {
            configCheckBoxes[i.key()] = new QCheckBox(i.key());
            ui->gridLayout_config->addWidget(configCheckBoxes[i.key()], row, col);
            if (++col == 5) {
                row++;
                col = 0;
            }
        }
    }
}

// setup versious items first time program runs
bool GarudaAssistant::setup() {
    this->setWindowTitle(tr("Garuda Assistant"));

    // Check if we are booted off a snapshot
    bash.setProgram("/bin/bash");
    bash.start();

    bool restoreSnapshot = handleSnapshotBoot(true, false);

    rootBash.start("pkexec", QStringList() << SCRIPT_PATH "rootshell.sh");
    if (runCmd("true", false, true, 0).exitCode) {
        displayError(tr("Authentication is required to run Garuda Assistant") + "\n" + tr("Exiting...."));
        return false;
    }

    if (!setTerminal())
        return false;

    // Save the state of snapper being installed since we have to check it so often
    hasSnapper = isInstalled("snapper");

    // Show or hide UI elements
    if (!isInstalled("gdm")) {
        ui->groupBox_gdm->hide();
    }

    ui->tabWidget->setTabVisible(ui->tabWidget->indexOf(ui->tab_subvolumes), ui->checkBox_show_subvolume->isChecked());

    ui->tabWidget->setTabVisible(ui->tabWidget->indexOf(ui->tab_snapper_settings), ui->checkBox_snapper_advanced->isChecked());

    ui->groupBox_snapper_config_edit->hide();

    if (!hasSnapper) {
        ui->checkBox_snapper_boot->hide();
        ui->checkBox_snapper_cleanup->hide();
        ui->checkBox_snapper_timeline->hide();
        ui->tabWidget->setTabVisible(ui->tabWidget->indexOf(ui->tab_snapper_general), false);
    }

    // Initialize combobox with list of shells
    ui->comboBox_shell->addItem("bash");
    ui->comboBox_shell->addItem("fish");
    ui->comboBox_shell->addItem("sh");
    ui->comboBox_shell->addItem("zsh");

    // Initialize the DNS combobox
    ui->comboBox_dns->addItem("");

    QMapIterator<QString, QString> i(dnsChoices);

    while (i.hasNext()) {
        i.next();
        ui->comboBox_dns->addItem(i.key());
    }

    // Create the config checkboxes
    setupConfigBoxes();

    refreshInxi();

    // connect the signal for the QProcesses
    connect(&diagProc, SIGNAL(readyReadStandardOutput()), this, SLOT(diagnosticsReturn()));
    connect(&inxiProc, SIGNAL(readyReadStandardOutput()), this, SLOT(inxiReturn()));

    // Connect all the checkboxes on the system components and settings tabs
    QSignalMapper *mapper = new QSignalMapper();
    connect(mapper, SIGNAL(mapped(QWidget *)), SLOT(on_checkBox_clicked(QWidget *)));

    auto checkboxList = ui->scrollArea_settings->findChildren<QCheckBox *>();
    checkboxList += ui->scrollArea_system->findChildren<QCheckBox *>();
    for (auto checkbox : checkboxList) {
        connect(checkbox, SIGNAL(clicked(bool)), mapper, SLOT(map()));
        mapper->setMapping(checkbox, checkbox);
    }

    refreshInterface();

    // The btrfs and snapper setup happens here because it has to happen after the root shell is created
    loadBTRFS();

    loadSnapper();
    if (snapperConfigs.contains("root"))
        ui->comboBox_snapper_configs->setCurrentText("root");
    populateSnapperGrid();
    populateSnapperConfigSettings();
    ui->pushButton_restore_snapshot->setEnabled(false);

    if (isSnapBoot)
        handleSnapshotBoot(false, restoreSnapshot);
    return true;
}

bool GarudaAssistant::checkAndInstall(QString package) {
    // If it's already installed, we are good to go
    if (isInstalled(package) || isInstalled(package + "-git"))
        return true;

    this->hide();
    runCmdTerminal("pacman -S " + package, true);

    this->show();
    return isInstalled(package);
}

void GarudaAssistant::displayError(QString errorText) { QMessageBox::critical(0, "Error", errorText); }

// Returns the current user's default shell
QString GarudaAssistant::getUserDefaultShell() {
    return runCmd("basename $(/usr/bin/getent passwd $USER | awk -F':' '{print $7}')", false, false).output;
}

// Populates servicesEnabledSet with a list of enabled services
void GarudaAssistant::loadEnabledUnits() {
    this->unitsEnabledSet.clear();

    QString bashOutput = runCmd("systemctl list-unit-files --state=enabled -q --no-pager | awk '{print $1}'", false, false).output;
    QStringList serviceList = bashOutput.split('\n');
    this->unitsEnabledSet = QSet<QString>(serviceList.begin(), serviceList.end());

    return;
}

// Populates Global servicesEnabledSet with a list of enabled services
void GarudaAssistant::loadGlobalEnabledUnits() {
    this->globalUnitsEnabledSet.clear();

    QString bashOutput = runCmd("systemctl --global list-unit-files --state=enabled -q --no-pager | awk '{print $1}'", false, false).output;
    QStringList globalserviceList = bashOutput.split('\n');
    this->globalUnitsEnabledSet = QSet<QString>(globalserviceList.begin(), globalserviceList.end());

    return;
}

// Get a list of groups for the current user
// We needs the groups that are set in /etc/group, not the currently active groups
void GarudaAssistant::loadGroups() {
    this->groupsEnabled.clear();

    // Load the data from /etc/group
    Result cmdResult = runCmd("getent group | grep $USER | awk -F: '{print $1\":\"$4}'", false, false);
    if (cmdResult.exitCode != 0) {
        displayError(tr("Failed to load groups for current user"));
        return;
    }

    // We will need the username to compare it to
    QString currentUser = qgetenv("USER");

    // Now we iterate over each line finding groups with exact matches
    QStringList groupEntries = cmdResult.output.split('\n');
    for (int i = 0; i < groupEntries.size(); ++i) {
        QStringList groupEntry = groupEntries.at(i).split(':');
        if (groupEntry.at(1) != "") {
            QStringList users = groupEntry.at(1).split(',');
            for (int j = 0; j < users.size(); ++j) {
                if (users.at(j) == currentUser)
                    this->groupsEnabled.insert(groupEntry.at(0));
            }
        }
    }
    return;
}

// If the service state is different than the desired state, enable or disable it
void GarudaAssistant::setUnitState(QString service, bool enable, bool global) {
    QString globalString;
    if (global)
        globalString += " --global";
    if (service != "" && unitsEnabledSet.contains(service) != enable) {
        if (enable)
            runCmd("systemctl enable" + globalString + " --now --force " + service, false, true);
        else
            runCmd("systemctl disable" + globalString + " --now " + service, false, true);
    }
}

// Returns true if packageName is installed and false if not
bool GarudaAssistant::isInstalled(QString packageName) { return runCmd("pacman -Qq " + packageName, false, false).output == packageName; }

// Returns the name of the dns service or an empty string if an unknown DNS server is in use
QString GarudaAssistant::getCurrentDNS() {
    QString nameServer = runCmd("cat /etc/resolv.conf | grep -A1 nameserver | awk '/nameserver/ { print $NF }'", false, false).output;
    return dnsChoices.key(nameServer, "");
}

// If /etc/hosts contains more than 1 "Blocked Domains" line hblock must be enabled
bool GarudaAssistant::checkHblock() {
    QString hblock = runCmd("cat /etc/hosts | grep -A1 \"Blocked domains\" | awk '/Blocked domains/ { print $NF }'", false, false).output;
    return hblock.toInt() > 1;
}

bool GarudaAssistant::checkGDMWayland() {
    QString gdm = runCmd("cat /etc/gdm/custom.conf | grep -A1 WaylandEnable | awk '/WaylandEnable/ { print $NF }'", false, false).output;
    return !(gdm == "WaylandEnable=false");
}

// Updates the checkboxes and comboboxes with values from the system
void GarudaAssistant::refreshInterface() {
    loadEnabledUnits();
    loadGlobalEnabledUnits();
    loadGroups();

    // Loop through the checkboxes
    auto checkboxList = ui->scrollArea_settings->findChildren<QCheckBox *>();
    checkboxList += ui->scrollArea_system->findChildren<QCheckBox *>();
    checkboxList += ui->scrollArea_btrfs->findChildren<QCheckBox *>();
    for (auto checkbox : checkboxList) {
        if (checkbox->property("actionType") == "package") {
            checkbox->setChecked(isInstalled(checkbox->property("actionData").toString()));
        } else if (checkbox->property("actionType") == "global_service") {
            checkbox->setChecked(this->globalUnitsEnabledSet.contains(checkbox->property("actionData").toString()));
        } else if (checkbox->property("actionType") == "service") {
            checkbox->setChecked(this->unitsEnabledSet.contains(checkbox->property("actionData").toString()));
        } else if (checkbox->property("actionType") == "group") {
            checkbox->setChecked(this->groupsEnabled.contains(checkbox->property("actionData").toString()));
        }
    }

    ui->comboBox_dns->setCurrentText(getCurrentDNS());

    ui->checkBox_hblock->setChecked(checkHblock());

    ui->checkBox_gdm->setChecked(checkGDMWayland());

    ui->comboBox_shell->setCurrentText(getUserDefaultShell());
}

/*######################################################################################
 *                                  Maintenance tab                                    *
######################################################################################*/

void GarudaAssistant::on_pushButton_reflector_clicked() {
    this->hide();
    runCmdTerminal("reflector-simple", false);
    this->show();
}

void GarudaAssistant::on_pushButton_refreshkeyring_clicked() {
    this->hide();
    // This command does like nothing to fix any actual issues tho, neither does it update the keyring??
    runCmdTerminal("pacman-key --init && pacman -Qs archlinux-keyring && pacman-key "
                   "--populate archlinux; pacman -Qs chaotic-keyring && pacman-key --populate chaotic; pacman -Qs "
                   "blackarch-keyring && pacman-key --populate blackarch",
                   true);
    this->show();
}

void GarudaAssistant::on_pushButton_sysup_clicked() {
    this->hide();
    runCmdTerminal("update");
    this->show();
}

void GarudaAssistant::on_pushButton_orphans_clicked() {
    this->hide();
    runCmdTerminal("pacman -Rns $(pacman -Qtdq)", true);
    this->show();
}

void GarudaAssistant::on_pushButton_clrcache_clicked() {
    this->hide();
    runCmdTerminal("paccache -ruk 0", true);
    this->show();
}

void GarudaAssistant::on_pushButton_reinstall_clicked() {
    this->hide();
    runCmdTerminal("pacman -S $(pacman -Qnq)", true);
    this->show();
}

void GarudaAssistant::on_pushButton_dblck_clicked() {
    runCmd("rm /var/lib/pacman/db.lck", false, true);
    QMessageBox::information(this, tr("Garuda Asssistant"), tr("Pacman database lock removed!"));
}

void GarudaAssistant::on_pushButton_editrepo_clicked() {
    this->hide();
    runCmd("pace", false, false);
    this->show();
}

void GarudaAssistant::on_pushButton_clrlogs_clicked() { runCmdTerminal(SCRIPT_PATH "clear-cache.sh", true); }

void GarudaAssistant::on_pushButton_resetconfigs_clicked() {
    bool isChanged = false;
    QHashIterator<QString, QCheckBox *> i(configCheckBoxes);
    while (i.hasNext()) {
        i.next();
        if (i.value()->isChecked()) {
            runCmd(configActions[i.key()].command, false, false);
            isChanged = true;
        }
    }

    if (isChanged)
        QMessageBox::information(0, tr("Garuda Asssistant"), tr("Configs applied, please relogin to finish!"));
}

/*######################################################################################
 *                                    BTRFS tab                                        *
######################################################################################*/

// Returns a list of btrfs filesystems
QStringList GarudaAssistant::getBTRFSFilesystems() {
    return runCmd("btrfs filesystem show -m | grep uuid | awk -F':' '{gsub(/ /,\"\");print $3}'", false, true).output.split('\n');
}

// Returns one of the mountpoints for a given UUID
QString GarudaAssistant::findMountpoint(QString uuid) {
    return runCmd("findmnt --real -rno target,uuid | grep " + uuid + " | head -n 1 | awk '{print $1}'", false, false).output;
}

// Populates the btrfs fs structure
// TODO: Make this less fragile
void GarudaAssistant::loadBTRFS() {
    fsMap.clear();
    ui->comboBox_btrfsdevice->clear();

    QStringList uuidList = getBTRFSFilesystems();

    for (auto uuid : uuidList) {
        QString mountpoint = findMountpoint(uuid);
        if (!mountpoint.isEmpty()) {
            Btrfs btrfs = {};
            btrfs.mountPoint = mountpoint;
            QStringList usageLines = runCmd("LANG=C ; btrfs fi usage -b " + mountpoint, false, false).output.split('\n');
            for (QString line : usageLines) {
                QString type = line.split(':').at(0).trimmed();
                if (type == "Device size") {
                    btrfs.totalSize = line.split(':').at(1).trimmed().toLong();
                } else if (type == "Device allocated") {
                    btrfs.allocatedSize = line.split(':').at(1).trimmed().toLong();
                } else if (type == "Used") {
                    btrfs.usedSize = line.split(':').at(1).trimmed().toLong();
                } else if (type == "Free (estimated)") {
                    btrfs.freeSize = line.split(':').at(1).split(QRegExp("\\s+"), Qt::SkipEmptyParts).at(0).trimmed().toLong();
                } else if (type.startsWith("Data,")) {
                    btrfs.dataSize = line.split(':').at(2).split(',').at(0).trimmed().toLong();
                    btrfs.dataUsed = line.split(':').at(3).split(' ').at(0).trimmed().toLong();
                } else if (type.startsWith("Metadata,")) {
                    btrfs.metaSize = line.split(':').at(2).split(',').at(0).trimmed().toLong();
                    btrfs.metaUsed = line.split(':').at(3).split(' ').at(0).trimmed().toLong();
                } else if (type.startsWith("System,")) {
                    btrfs.sysSize = line.split(':').at(2).split(',').at(0).trimmed().toLong();
                    btrfs.sysUsed = line.split(':').at(3).split(' ').at(0).trimmed().toLong();
                }
            }
            fsMap[uuid] = btrfs;
            ui->comboBox_btrfsdevice->addItem(uuid);
        }
    }

    populateBtrfsUi(ui->comboBox_btrfsdevice->currentText());
    reloadSubvolList(ui->comboBox_btrfsdevice->currentText());
}

QString GarudaAssistant::toHumanReadable(double number) {
    int i = 0;
    const QVector<QString> units = {"B", "kiB", "MiB", "GiB", "TiB", "PiB", "EiB", "ZiB", "YiB"};
    while (number > 1024) {
        number /= 1024;
        i++;
    }
    return QString::number(number) + " " + units[i];
}

void GarudaAssistant::populateBtrfsUi(QString uuid) {
    // For the tools section
    int dataPercent = ((double)fsMap[uuid].dataUsed / fsMap[uuid].dataSize) * 100;
    ui->progressBar_btrfsdata->setValue(dataPercent);
    ui->progressBar_btrfsmeta->setValue(((double)fsMap[uuid].metaUsed / fsMap[uuid].metaSize) * 100);
    ui->progressBar_btrfssys->setValue(((double)fsMap[uuid].sysUsed / fsMap[uuid].sysSize) * 100);
    if (dataPercent < 85) {
        ui->label_balance->setText(tr("Balance recommended.  Click here ---->"));
    } else {
        ui->label_balance->setText(tr("Balance not needed at this time"));
    }

    // The information section
    ui->label_btrfsallocated->setText(toHumanReadable(fsMap[uuid].allocatedSize));
    ui->label_btrfsused->setText(toHumanReadable(fsMap[uuid].usedSize));
    ui->label_btrfssize->setText(toHumanReadable(fsMap[uuid].totalSize));
    ui->label_btrfsfree->setText(toHumanReadable(fsMap[uuid].freeSize));
    float freePercent = (double)fsMap[uuid].allocatedSize / fsMap[uuid].totalSize;
    if (freePercent < 0.70) {
        ui->label_btrfsmessage->setText(tr("You have lots of free space, did you overbuy?"));
    } else if (freePercent > 0.95) {
        ui->label_btrfsmessage->setText(tr("Situation critical!  Time to delete some data or buy more disk"));
    } else {
        ui->label_btrfsmessage->setText(tr("Your disk space is well utilized"));
    }
}

void GarudaAssistant::on_pushButton_load_clicked() {
    loadBTRFS();

    ui->pushButton_load->clearFocus();
}

void GarudaAssistant::on_pushButton_loadsubvol_clicked() {
    QString uuid = ui->comboBox_btrfsdevice->currentText();

    if (uuid.isEmpty()) {
        displayError(tr("No device selected") + "\n" + tr("Please Select a device first"));
        return;
    }

    reloadSubvolList(uuid);

    ui->pushButton_loadsubvol->clearFocus();
}

void GarudaAssistant::reloadSubvolList(QString uuid) {
    if (!fsMap.contains(uuid))
        return;

    fsMap[uuid].subVolumes.clear();

    QString mountpoint = findMountpoint(uuid);

    QStringList output = runCmd("btrfs subvolume list " + mountpoint, false, true).output.split('\n');
    QMap<QString, QString> subvols;
    for (QString line : output) {
        if (!line.isEmpty())
            subvols[line.split(' ').at(1)] = line.split(' ').at(8);
    }

    fsMap[uuid].subVolumes = subvols; // TODO: Use a pointer to avoid this copy operation

    populateSubvolList(uuid);
}

void GarudaAssistant::populateSubvolList(QString uuid) {
    ui->listWidget_subvols->clear();

    if (uuid.isEmpty() || fsMap[uuid].subVolumes.size() <= 0)
        return;

    QMapIterator<QString, QString> i(fsMap[uuid].subVolumes);

    bool includeSnaps = ui->checkBox_includesnapshots->isChecked();

    while (i.hasNext()) {
        i.next();
        if (includeSnaps || !(isTimeshift(i.value()) || isSnapper(i.value())))
            ui->listWidget_subvols->addItem(i.value());
    }
    ui->listWidget_subvols->sortItems();
}

void GarudaAssistant::on_checkBox_includesnapshots_clicked() { populateSubvolList(ui->comboBox_btrfsdevice->currentText()); }

void GarudaAssistant::on_pushButton_applybtrfs_clicked() {
    QStringList cmdList;

    auto checkboxList = ui->scrollArea_btrfs->findChildren<QCheckBox *>();
    for (auto checkbox : checkboxList) {
        QString service = checkbox->property("actionData").toString();
        if (service != "" && unitsEnabledSet.contains(service) != checkbox->isChecked()) {
            if (checkbox->isChecked())
                cmdList.append("systemctl enable --now --force " + service);
            else
                cmdList.append("systemctl disable --now " + service);
        }
    }

    runCmd(cmdList, false, true);

    QMessageBox::information(0, tr("Garuda Assistant"), tr("Changes applied"));

    ui->pushButton_applybtrfs->clearFocus();
}

// Delete a subvolume after checking for a variety of errors
void GarudaAssistant::on_pushButton_deletesubvol_clicked() {
    QString subvol = ui->listWidget_subvols->currentItem()->text();
    QString uuid = ui->comboBox_btrfsdevice->currentText();

    // Make sure the everything is good in the UI
    if (subvol.isEmpty() || uuid.isEmpty()) {
        displayError(tr("Nothing to delete!"));
        ui->pushButton_deletesubvol->clearFocus();
        return;
    }

    // get the subvolid, if it isn't found abort
    QString subvolid = fsMap[uuid].subVolumes.key(subvol);
    if (subvolid.isEmpty()) {
        displayError(tr("Failed to delete subvolume!") + "\n\n" + tr("subvolid missing from map"));
        ui->pushButton_deletesubvol->clearFocus();
        return;
    }

    // ensure the subvol isn't mounted, btrfs will delete a mounted subvol but we probably shouldn't
    if (isMounted(uuid, subvolid)) {
        displayError(tr("You cannot delete a mounted subvolume") + "\n\n" + tr("Please unmount the subvolume before continuing"));
        ui->pushButton_deletesubvol->clearFocus();
        return;
    }

    Result result;

    // First let's see if this is a timeshift snapshot, if it is, we need to use timeshift to remove it
    if (isTimeshift(subvol) && isInstalled("timeshift")) {
        QString snapshot = subvol.split('/').at(2);
        // Let's het confirmation first
        if (QMessageBox::question(0, tr("Please Confirm"),
                                  tr("You are about to delete all subvolumes associated with timeshift snapshot ") + snapshot + "\n\n" +
                                      tr("Are you sure you want to proceed?")) != QMessageBox::Yes)
            return;

        result = runCmd("/usr/bin/timeshift --delete --snapshot '" + snapshot + "'", true, true);
    } else if (isSnapper(subvol) && hasSnapper) {
        QMessageBox::information(0, tr("Snapshot Delete"),
                                 tr("That subvolume is a snapper shapshot") + "\n\n" + tr("Please use the snapper tab to remove it"));
        return;
    } else {
        // Everything looks good so far, now we put up a confirmation box
        if (QMessageBox::question(0, tr("Confirm"), tr("Are you sure you want to delete ") + subvol) != QMessageBox::Yes)
            return;

        QString mountpoint = mountRoot(uuid);

        // Everything checks out, lets delete the subvol
        if (mountpoint.right(1) != "/")
            mountpoint += "/";
        result = runCmd("btrfs subvolume delete " + mountpoint + subvol, true, true);
    }

    if (result.exitCode == 0) {
        reloadSubvolList(uuid);
    } else
        displayError(tr("Process failed with output:") + "\n\n");

    ui->pushButton_deletesubvol->clearFocus();
}

QString GarudaAssistant::mountRoot(QString uuid) {
    // Make sure the root is mounted so we can delete the subvol
    QStringList findmntOutput = runCmd("findmnt -nO subvolid=5 -o uuid,target | head -n 1", false, false).output.split('\n');
    QString mountpoint = "";
    QString command = "";
    for (QString line : findmntOutput) {
        if (line.split(' ').at(0).trimmed() == uuid)
            mountpoint = line.split(' ').at(1).trimmed();
    }

    // If it isn't mounted we need to mount it
    if (mountpoint.isEmpty()) {
        mountpoint = "/tmp/" + QUuid::createUuid().toString();
        runCmd("mkdir " + mountpoint, false, true);
        runCmd("mount -t btrfs -o subvolid=5 UUID=" + uuid + " " + mountpoint, false, true);
    }

    return mountpoint;
}
void GarudaAssistant::on_comboBox_btrfsdevice_activated() {
    QString device = ui->comboBox_btrfsdevice->currentText();
    if (!device.isEmpty() && fsMap[device].totalSize != 0) {
        populateBtrfsUi(device);
        reloadSubvolList(device);
    }
    ui->comboBox_btrfsdevice->clearFocus();
}

void GarudaAssistant::on_pushButton_balance_clicked() {
    this->hide();
    runCmdTerminal("btrfs balance start / --full-balance --verbose", true);
    this->show();
}

void GarudaAssistant::on_checkBox_show_subvolume_clicked(bool checked) {
    ui->tabWidget->setTabVisible(ui->tabWidget->indexOf(ui->tab_subvolumes), checked);
}

bool GarudaAssistant::isTimeshift(QString subvolume) { return subvolume.contains("timeshift-btrfs"); }

bool GarudaAssistant::isSnapper(QString subvolume) { return subvolume.contains(".snapshots") && !subvolume.endsWith(".snapshots"); }

// Restores a snapper snapshot after extensive error checking
void GarudaAssistant::restoreSnapshot(QString uuid, QString subvolume) {
    // Make sure subvolume doesn't have a leading slash
    if (subvolume.startsWith("/"))
        subvolume = subvolume.right(subvolume.length() - 1);

    if (isTimeshift(subvolume)) {
        QMessageBox::warning(0, tr("Timeshift Snapshot"), tr("Please Timeshift to restore this snapshot"));
        return;
    }

    if (!isSnapper(subvolume)) {
        displayError(tr("This is not a snapshot that can be restored by this application"));
        return;
    }

    // get the subvolid, if it isn't found abort
    QString subvolid = fsMap[uuid].subVolumes.key(subvolume);
    if (subvolid.isEmpty()) {
        displayError(tr("Failed to restore snapshot!"));
        return;
    }

    // Now we need to find out what the target for the restore is
    QString prefix = subvolume.split(".snapshots").at(0);

    // If the prefix is empty, that means that we are trying to restore the subvolume mounted as /
    // It also means the subvolume is currently mounted so we can't restore it
    if (prefix.isEmpty()) {
        displayError(tr("Can't restore snapshot while the target subvolume is mounted") + "\n\n" +
                     tr("Unmount the volume, reboot off a snapshot or use this tool from a live ISO to complete the restore"));
        return;
    }

    // Strip the trailing /
    QString targetSubvolume = prefix.left(prefix.length() - 1);

    // Get the subvolid of the target and do some additional error checking
    QString targetSubvolid = fsMap[uuid].subVolumes.key(targetSubvolume);
    if (targetSubvolid.isEmpty()) {
        displayError(tr("Target not found"));
        return;
    }

    if (isMounted(uuid, targetSubvolid)) {
        displayError(tr("Can't restore snapshot while the target subvolume is mounted"));
        return;
    }

    // We are out of errors to check for, time to ask for confirmation
    if (QMessageBox::question(0, tr("Confirm"),
                              tr("Are you sure you want to restore ") + subvolume + tr(" to ", "as in from/to") + targetSubvolume) !=
        QMessageBox::Yes)
        return;

    // Ensure the root of the partition is mounted and get the mountpoint
    QString mountpoint = mountRoot(uuid);

    // Make sure we have a trailing /
    if (mountpoint.right(1) != "/")
        mountpoint += "/";

    // We are out of excuses, time to do the restore....carefully
    QString targetBackup = "restore_backup_" + targetSubvolume + "_" + QTime::currentTime().toString("HHmmsszzz");

    QDir dirWorker;

    // Rename the target
    runCmd("mv " + mountpoint + targetSubvolume + " " + mountpoint + targetBackup, false, true);

    if (!dirWorker.exists(mountpoint + targetBackup)) {
        displayError(tr("Failed to make a backup of target subvolume"));
        return;
    }

    // We moved the snapshot so we need to change the location
    QString newSubvolume = targetBackup + subvolume.right(subvolume.length() - targetSubvolume.length());

    // Place a snapshot of the source where the target was
    runCmd("btrfs subvolume snapshot " + mountpoint + newSubvolume + " " + mountpoint + targetSubvolume, false, true);

    // Make sure it worked
    if (!dirWorker.exists(mountpoint + targetSubvolume)) {
        // That failed, try to put the old one back
        runCmd("/usr/bin/mv " + mountpoint + targetBackup + " " + mountpoint + targetSubvolume, false, true);
        displayError(tr("Failed to restore subvolume!") + "\n\n" + tr("Please verify the status of your system before rebooting"));
        return;
    }

    // The restore was successful, now we need to move the snapshots into the target
    runCmd("mv " + mountpoint + targetBackup + "/.snapshots" + " " + mountpoint + targetSubvolume + "/.", false, true);

    if (!dirWorker.exists(mountpoint + targetSubvolume + "/.snapshots")) {
        // If this fails, not much can be done except let the user know
        displayError(tr("The restore was successful but the migration of the snapshots failed") + "\n\n" +
                     tr("Please migrate the .snapshots subvolume manually"));
        return;
    }

    // If we get here I guess it worked
    QMessageBox::information(0, tr("Snapshot Restore"),
                             tr("Snapshot restoration complete.") + "\n\n" + tr("A copy of the original subvolume has been saved as ") +
                                 targetBackup + "\n\n" + tr("Please verify before rebooting"));
}

bool GarudaAssistant::isMounted(QString uuid, QString subvolid) {
    return uuid == runCmd("findmnt -nO subvolid=" + subvolid.trimmed() + " -o uuid | head -n 1", false, false).output.trimmed();
}

/*######################################################################################
 *                           Snapper tabs                                              *
######################################################################################*/

// Loads the snapper configs and snapshots
void GarudaAssistant::loadSnapper() {
    // If snapper isn't installed, no need to continue
    if (!hasSnapper)
        return;

    // Load the list of valid configs
    ui->comboBox_snapper_configs->clear();
    ui->comboBox_snapper_config_settings->clear();
    snapperConfigs.clear();
    snapperSnapshots.clear();
    QString outputList = runCmd("snapper list-configs | tail -n +3", false, true).output;

    if (outputList.isEmpty())
        return;

    for (QString line : outputList.split('\n')) {
        // for each config, add to the map and add it's snapshots to the vector
        QString name = line.split('|').at(0).trimmed();
        snapperConfigs[name] = line.split('|').at(1).trimmed();
        ui->comboBox_snapper_configs->addItem(name);
        ui->comboBox_snapper_config_settings->addItem(name);

        // If we are booted off the snapshot we need to handle the root snapshots manually
        if (name == "root" && isSnapBoot) {
            QString output = runCmd("LANG=C findmnt -no uuid,options /", false, false).output;
            if (output.isEmpty())
                continue;

            QString uuid = output.split(' ').at(0).trimmed();
            QString options = output.right(output.length() - uuid.length()).trimmed();
            if (options.isEmpty() || uuid.isEmpty())
                continue;

            QString subvol;
            for (QString option : options.split(',')) {
                if (option.startsWith("subvol="))
                    subvol = option.split("subvol=").at(1);
            }

            if (subvol.isEmpty() || !subvol.contains(".snapshots"))
                continue;

            // Make sure subvolume doesn't have a leading slash
            if (subvol.startsWith("/"))
                subvol = subvol.right(subvol.length() - 1);

            if (!isSnapper(subvol))
                continue;

            // get the subvolid, if it isn't found abort
            QString subvolid = fsMap[uuid].subVolumes.key(subvol);
            if (subvolid.isEmpty())
                continue;

            // Now we need to find out where the snapshots are actually stored
            QString prefix = subvol.split(".snapshots").at(0);

            // It shouldn't be possible for the prefix to empty when booted off a snapshot but we check anyway
            if (prefix.isEmpty())
                continue;

            // Make sure the root of the partition is mounted
            QString mountpoint = mountRoot(uuid);

            // Make sure we have a trailing /
            if (mountpoint.right(1) != "/")
                mountpoint += "/";

            QString findOutput = runCmd("find " + mountpoint + prefix + ".snapshots -maxdepth 2 -name info.xml", false, true).output;

            for (QString fileName : findOutput.split('\n')) {
                SnapperSnapshots snap = getSnapperMeta(fileName);
                if (snap.number == 0)
                    snapperSnapshots[name].append(snap);
            }
        } else {
            QString list = runCmd("snapper -c " + name + " list --columns number,date,description | tail -n +4", false, true).output;
            if (list.isEmpty())
                continue;
            for (QString snap : list.split('\n'))
                snapperSnapshots[name].append(
                    {snap.split('|').at(0).trimmed().toInt(), snap.split('|').at(1).trimmed(), snap.split('|').at(2).trimmed()});
        }
    }
}

SnapperSnapshots GarudaAssistant::getSnapperMeta(QString filename) {
    SnapperSnapshots snap;
    snap.number = 0;
    QString xmlOutput = runCmd("/usr/bin/cat " + filename, false, true).output;
    if (xmlOutput.isEmpty())
        return snap;

    for (QString line : xmlOutput.split('\n')) {
        if (line.trimmed().startsWith("<num>"))
            snap.number = line.trimmed().split("<num>").at(1).split("</num>").at(0).trimmed().toInt();
        else if (line.trimmed().startsWith("<date>"))
            snap.time = line.trimmed().split("<date>").at(1).split("</date>").at(0).trimmed();
        else if (line.trimmed().startsWith("<description>"))
            snap.desc = line.trimmed().split("<description>").at(1).split("</description>").at(0).trimmed();
    }

    return snap;
}

void GarudaAssistant::populateSnapperGrid() {
    if (ui->checkBox_snapper_restore->isChecked()) {
        QString config = ui->comboBox_snapper_configs->currentText();

        // Clear the table and set the headers
        ui->tableWidget_snapper->clear();
        ui->tableWidget_snapper->setColumnCount(3);
        ui->tableWidget_snapper->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Subvolume")));
        ui->tableWidget_snapper->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Date/Time")));
        ui->tableWidget_snapper->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Description")));

        // Make sure there is something to populate
        if (snapperSubvolumes[config].isEmpty())
            return;

        // Populate the table
        ui->tableWidget_snapper->setRowCount(snapperSubvolumes[config].size());
        for (int i = 0; i < snapperSubvolumes[config].size(); i++) {
            QTableWidgetItem *subvol = new QTableWidgetItem(snapperSubvolumes[config].at(i).subvol);
            ui->tableWidget_snapper->setItem(i, 0, subvol);
            ui->tableWidget_snapper->setItem(i, 1, new QTableWidgetItem(snapperSubvolumes[config].at(i).time));
            ui->tableWidget_snapper->setItem(i, 2, new QTableWidgetItem(snapperSubvolumes[config].at(i).desc));
        }
    } else {
        QString config = ui->comboBox_snapper_configs->currentText();

        // Clear the table and set the headers
        ui->tableWidget_snapper->clear();
        ui->tableWidget_snapper->setColumnCount(3);
        ui->tableWidget_snapper->setHorizontalHeaderItem(0, new QTableWidgetItem(tr("Number", "The number associated with a snapshot")));
        ui->tableWidget_snapper->setHorizontalHeaderItem(1, new QTableWidgetItem(tr("Date/Time")));
        ui->tableWidget_snapper->setHorizontalHeaderItem(2, new QTableWidgetItem(tr("Description")));

        // Make sure there is something to populate
        if (snapperSnapshots[config].isEmpty())
            return;

        // Populate the table
        ui->tableWidget_snapper->setRowCount(snapperSnapshots[config].size());
        for (int i = 0; i < snapperSnapshots[config].size(); i++) {
            QTableWidgetItem *number = new QTableWidgetItem(snapperSnapshots[config].at(i).number);
            number->setData(Qt::DisplayRole, snapperSnapshots[config].at(i).number);
            ui->tableWidget_snapper->setItem(i, 0, number);
            ui->tableWidget_snapper->setItem(i, 1, new QTableWidgetItem(snapperSnapshots[config].at(i).time));
            ui->tableWidget_snapper->setItem(i, 2, new QTableWidgetItem(snapperSnapshots[config].at(i).desc));
        }
    }

    // Resize the colums to make everything fit
    ui->tableWidget_snapper->resizeColumnsToContents();
    ui->tableWidget_snapper->sortItems(0, Qt::DescendingOrder);
}

// Repopulate the grid when a different config is selected
void GarudaAssistant::on_comboBox_snapper_configs_activated() {
    populateSnapperGrid();
    ui->comboBox_snapper_configs->clearFocus();
}

void GarudaAssistant::on_pushButton_snapper_create_clicked() {
    QString config = ui->comboBox_snapper_configs->currentText();

    // If snapper isn't installed, we should bail
    if (!hasSnapper)
        return;

    // This shouldn't be possible but we check anyway
    if (config.isEmpty()) {
        displayError(tr("No config selected for snapshot"));
        return;
    }

    // OK, let's go ahead and take the snapshot
    runCmd("snapper -c " + config + " create -d 'Manual Snapshot'", false, true);

    loadSnapper();
    ui->comboBox_snapper_configs->setCurrentText(config);
    populateSnapperGrid();

    ui->pushButton_snapper_create->clearFocus();
}

void GarudaAssistant::on_pushButton_snapper_delete_clicked() {
    if (ui->tableWidget_snapper->currentRow() == -1) {
        displayError(tr("Nothing selected!"));
        return;
    }

    QList<QTableWidgetItem *> list = ui->tableWidget_snapper->selectedItems();

    QSet<QString> numbers;

    for (QTableWidgetItem *item : list) {
        numbers.insert(ui->tableWidget_snapper->item(item->row(), 0)->text());
    }

    // Ask for confirmation
    if (QMessageBox::question(0, tr("Confirm"), tr("Are you sure you want to delete the selected snapshot(s)?")) != QMessageBox::Yes)
        return;

    QString config = ui->comboBox_snapper_configs->currentText();

    for (QString number : numbers) {
        // This shouldn't be possible but we check anyway
        if (config.isEmpty() || number.isEmpty()) {
            displayError(tr("Cannot delete snapshot"));
            return;
        }

        // Delete the snapshot
        runCmd("sudo snapper -c " + config + " delete " + number, false, true);
    }

    // Reload the UI since something changed
    loadSnapper();
    ui->comboBox_snapper_configs->setCurrentText(config);
    populateSnapperGrid();

    ui->pushButton_snapper_delete->clearFocus();
}

void GarudaAssistant::populateSnapperConfigSettings() {
    QString name = ui->comboBox_snapper_config_settings->currentText();
    if (name.isEmpty())
        return;

    QString output = runCmd("sudo snapper -c " + name + " get-config | tail -n +3", false, true).output;

    if (output.isEmpty())
        return;

    ui->label_snapper_config_name->setText(name);
    for (QString line : output.split('\n')) {
        if (line.isEmpty())
            continue;
        QString key = line.split('|').at(0).trimmed();
        QString value = line.split('|').at(1).trimmed();
        if (key == "SUBVOLUME")
            ui->label_snapper_backup_path->setText(value);
        else if (key == "TIMELINE_CREATE")
            ui->checkBox_snapper_enabletimeline->setChecked(value.toStdString() == "yes");
        else if (key == "TIMELINE_LIMIT_HOURLY")
            ui->spinBox_snapper_hourly->setValue(value.toInt());
        else if (key == "TIMELINE_LIMIT_DAILY")
            ui->spinBox_snapper_daily->setValue(value.toInt());
        else if (key == "TIMELINE_LIMIT_WEEKLY")
            ui->spinBox_snapper_weekly->setValue(value.toInt());
        else if (key == "TIMELINE_LIMIT_MONTHLY")
            ui->spinBox_snapper_monthly->setValue(value.toInt());
        else if (key == "TIMELINE_LIMIT_YEARLY")
            ui->spinBox_snapper_yearly->setValue(value.toInt());
        else if (key == "NUMBER_LIMIT")
            ui->spinBox_snapper_pacman->setValue(value.toInt());
    }

    snapperTimelineEnable(ui->checkBox_snapper_enabletimeline->isChecked());
}

// Enables or disables the timeline spinboxes to match the timeline checkbox
void GarudaAssistant::snapperTimelineEnable(bool enable) {
    if (enable) {
        ui->spinBox_snapper_hourly->setEnabled(true);
        ui->spinBox_snapper_daily->setEnabled(true);
        ui->spinBox_snapper_weekly->setEnabled(true);
        ui->spinBox_snapper_monthly->setEnabled(true);
        ui->spinBox_snapper_yearly->setEnabled(true);
    } else {
        ui->spinBox_snapper_hourly->setEnabled(false);
        ui->spinBox_snapper_daily->setEnabled(false);
        ui->spinBox_snapper_weekly->setEnabled(false);
        ui->spinBox_snapper_monthly->setEnabled(false);
        ui->spinBox_snapper_yearly->setEnabled(false);
    }
}

void GarudaAssistant::on_checkBox_snapper_enabletimeline_clicked(bool checked) { snapperTimelineEnable(checked); }

// When a new config selected repopulate the UI
void GarudaAssistant::on_comboBox_snapper_config_settings_activated() {
    populateSnapperConfigSettings();

    ui->comboBox_snapper_config_settings->clearFocus();
}

void GarudaAssistant::on_pushButton_snapper_save_config_clicked() {
    QString name;

    // If the settings box is visible we are changing settings on an existing config
    if (ui->groupBox_snapper_config_settings->isVisible()) {
        name = ui->comboBox_snapper_config_settings->currentText();
        if (name.isEmpty()) {
            displayError(tr("Failed to save changes"));
            ui->pushButton_snapper_save_config->clearFocus();
            return;
        }

        QString command = "snapper -c " + name + " set-config ";
        command += "\"TIMELINE_CREATE=" + QString(ui->checkBox_snapper_enabletimeline->isChecked() ? "yes" : "no") + "\"";
        command += " \"TIMELINE_LIMIT_HOURLY=" + QString::number(ui->spinBox_snapper_hourly->value()) + "\"";
        command += " \"TIMELINE_LIMIT_DAILY=" + QString::number(ui->spinBox_snapper_daily->value()) + "\"";
        command += " \"TIMELINE_LIMIT_WEEKLY=" + QString::number(ui->spinBox_snapper_weekly->value()) + "\"";
        command += " \"TIMELINE_LIMIT_MONTHLY=" + QString::number(ui->spinBox_snapper_monthly->value()) + "\"";
        command += " \"TIMELINE_LIMIT_YEARLY=" + QString::number(ui->spinBox_snapper_yearly->value()) + "\"";
        command += " \"NUMBER_LIMIT=" + QString::number(ui->spinBox_snapper_pacman->value()) + "\"";

        runCmd(command, false, true);

        QMessageBox::information(0, tr("Snapper"), tr("Changes saved"));
    } else { // This is new config we are creating
        name = ui->lineEdit_snapper_name->text();

        // Remove any whitespace from name
        name = name.simplified().replace(" ", "");

        if (name.isEmpty()) {
            displayError(tr("Please enter a valid name"));
            ui->pushButton_snapper_save_config->clearFocus();
            return;
        }

        if (snapperConfigs.contains(name)) {
            displayError(tr("That name is already in use!"));
            ui->pushButton_snapper_save_config->clearFocus();
            return;
        }

        // Create the new config
        runCmd("snapper -c " + name + " create-config " + ui->comboBox_snapper_path->currentText(), false, true);

        // Reload the UI
        loadSnapper();
        ui->comboBox_snapper_config_settings->setCurrentText(name);
        populateSnapperGrid();
        populateSnapperConfigSettings();

        // Put the ui back in edit mode
        ui->groupBox_snapper_config_display->show();
        ui->groupBox_snapper_config_edit->hide();
        ui->groupBox_snapper_config_settings->show();
    }

    ui->pushButton_snapper_save_config->clearFocus();
}

// Switches the snapper config between edit config and new config mode
void GarudaAssistant::on_pushButton_snapper_new_config_clicked() {
    if (ui->groupBox_snapper_config_edit->isVisible()) {
        ui->lineEdit_snapper_name->clear();

        // Put the ui back in edit mode
        ui->groupBox_snapper_config_display->show();
        ui->groupBox_snapper_config_edit->hide();
        ui->groupBox_snapper_config_settings->show();

        ui->pushButton_snapper_new_config->setText(tr("New Config"));
        ui->pushButton_snapper_new_config->clearFocus();
    } else {
        // Get a list of btrfs mountpoints that could be backed up
        output = runCmd("findmnt --real -nlo FSTYPE,TARGET | grep \"^btrfs\" | awk '{print $2}'", false, false).output;

        if (output.isEmpty()) {
            displayError(tr("No btrfs subvolumes found"));
            return;
        }

        // Populate the list of mountpoints after checking that their isn't already a config
        ui->comboBox_snapper_path->clear();
        for (QString line : output.split('\n'))
            if (snapperConfigs.key(line.trimmed()).isEmpty())
                ui->comboBox_snapper_path->addItem(line.trimmed());

        // Put the UI in create config mode
        ui->groupBox_snapper_config_display->hide();
        ui->groupBox_snapper_config_edit->show();
        ui->groupBox_snapper_config_settings->hide();

        ui->pushButton_snapper_new_config->setText(tr("Cancel New Config"));
        ui->pushButton_snapper_new_config->clearFocus();
    }
}

void GarudaAssistant::on_pushButton_snapper_delete_config_clicked() {
    QString name = ui->comboBox_snapper_config_settings->currentText();

    if (name.isEmpty()) {
        displayError(tr("No config selected"));
        ui->pushButton_snapper_delete_config->clearFocus();
        return;
    }

    if (name == "root") {
        displayError(tr("You may not don't delete the root config"));
        ui->pushButton_snapper_delete_config->clearFocus();
        return;
    }

    // Ask for confirmation
    if (QMessageBox::question(0, tr("Please Confirm"),
                              tr("Are you sure you want to delete ") + name + "\n\n" + tr("This action cannot be undone")) !=
        QMessageBox::Yes) {
        ui->pushButton_snapper_delete_config->clearFocus();
        return;
    }

    // Delete the config
    runCmd("snapper -c " + name + " delete-config", false, true);

    // Reload the UI with the new list of configs
    loadSnapper();
    populateSnapperGrid();
    populateSnapperConfigSettings();

    ui->pushButton_snapper_delete_config->clearFocus();
}

// Show the snapper settings tab when the checkbox is checked
void GarudaAssistant::on_checkBox_snapper_advanced_clicked(bool checked) {
    ui->tabWidget->setTabVisible(ui->tabWidget->indexOf(ui->tab_snapper_settings), checked);
}

void GarudaAssistant::on_checkBox_snapper_restore_clicked(bool checked) {
    enableRestoreMode(checked);

    ui->checkBox_snapper_restore->clearFocus();
}

void GarudaAssistant::enableRestoreMode(bool enable) {
    ui->pushButton_snapper_create->setEnabled(!enable);
    ui->pushButton_snapper_delete->setEnabled(!enable);
    ui->pushButton_restore_snapshot->setEnabled(enable);

    if (enable) {
        ui->label_snapper_combo->setText(tr("Select Subvolume:"));
        ui->comboBox_snapper_configs->clear();
        ui->tableWidget_snapper->clear();
        loadSnapperRestoreMode();
        populateSnapperGrid();
    } else {
        ui->label_snapper_combo->setText(tr("Select Config:"));
        loadSnapper();
        populateSnapperGrid();
    }
}

void GarudaAssistant::on_pushButton_restore_snapshot_clicked() {
    // First lets double check to ensure we are in restore mode
    if (!ui->checkBox_snapper_restore->isChecked()) {
        displayError(tr("Please enter restore mode before trying to restore a snapshot"));
        return;
    }

    if (ui->tableWidget_snapper->currentRow() == -1) {
        displayError(tr("Nothing selected!"));
        return;
    }

    QString subvolName = ui->comboBox_snapper_configs->currentText();
    QString subvol = ui->tableWidget_snapper->item(ui->tableWidget_snapper->currentRow(), 0)->text();

    // These shouldn't be possible but check anyway
    if (!snapperSubvolumes.contains(subvolName) || snapperSubvolumes[subvolName].size() == 0) {
        displayError(tr("Failed to restore snapshot"));
        return;
    }

    // For a given subvol they all have the same uuid so we can just use the first one
    QString uuid = snapperSubvolumes[subvolName].at(0).uuid;

    restoreSnapshot(uuid, subvol);

    ui->pushButton_restore_snapshot->clearFocus();
}

// Verify we are booted off a snapshot and then offer to restore it
bool GarudaAssistant::handleSnapshotBoot(bool checkOnly, bool restore) {
    QString output = runCmd("LANG=C findmnt -no uuid,options /", false, false).output;
    if (output.isEmpty())
        return false;

    QString uuid = output.split(' ').at(0).trimmed();
    QString options = output.right(output.length() - uuid.length()).trimmed();
    if (options.isEmpty() || uuid.isEmpty())
        return false;

    QString subvol;
    for (QString option : options.split(',')) {
        if (option.startsWith("subvol="))
            subvol = option.split("subvol=").at(1);
    }

    if (subvol.isEmpty() || !subvol.contains(".snapshots"))
        return false;

    // If we get to here we should be booted off the snapshot stored in subvol
    isSnapBoot = true;

    // Ask the end user if they want to restore it
    if (checkOnly) {
        return QMessageBox::question(0, tr("Snapshot boot detected"),
                                     tr("You are currently booted into snapshot ") + subvol + "\n\n" +
                                         tr("Would you like to restore it?")) == QMessageBox::Yes;
    } else if (restore)
        restoreSnapshot(uuid, subvol);

    // No matter if we restored a snapshot a not, show the subvolume tab and switch to it
    ui->tabWidget->setTabVisible(ui->tabWidget->indexOf(ui->tab_snapper_general), true);
    ui->tabWidget->setCurrentIndex(ui->tabWidget->indexOf(ui->tab_snapper_general));
    ui->checkBox_snapper_restore->setChecked(true);
    enableRestoreMode(true);
    return false;
}

void GarudaAssistant::loadSnapperRestoreMode() {
    if (!ui->checkBox_snapper_restore->isChecked())
        return;

    snapperSubvolumes.clear();
    ui->comboBox_snapper_configs->clear();

    for (QString uuid : getBTRFSFilesystems()) {
        // First get a mountpoint associated with uuid
        QString output = runCmd("findmnt --real -nlo UUID,TARGET | grep " + uuid + " | head -n 1", false, false).output;

        if (output.isEmpty())
            continue;

        QString target = output.split(' ').at(1);

        if (target.isEmpty())
            continue;

        // Now we can get all the subvolumes tied to that mountpoint
        output = runCmd("btrfs subvolume list " + target, false, true).output;

        if (output.isEmpty())
            continue;

        // We need to ensure the root is mounted and get the mountpoint
        QString mountpoint = mountRoot(uuid);

        // Ensure it has a trailing /
        if (mountpoint.right(1) != "/")
            mountpoint += "/";

        for (QString line : output.split('\n')) {
            SnapperSubvolume subvol;
            if (line.isEmpty())
                continue;

            subvol.uuid = uuid;
            subvol.subvolid = line.split(' ').at(1).trimmed();
            subvol.subvol = line.split(' ').at(8).trimmed();

            // Check if it is snapper snapshot
            if (!subvol.subvol.contains(".snapshots") || subvol.subvol.endsWith(".snapshots"))
                continue;

            // It is a snapshot so now we parse it and read the snapper XML
            QString end = "snapshot";
            QString filename;

            // If the normal root is mounted the root snapshots will be at /.snapshots
            if (subvol.subvol.startsWith(".snapshots"))
                filename = "/" + subvol.subvol.left(subvol.subvol.length() - end.length()) + "info.xml";
            else
                filename = mountpoint + subvol.subvol.left(subvol.subvol.length() - end.length()) + "info.xml";

            SnapperSnapshots snap = getSnapperMeta(filename);

            if (snap.number == 0)
                continue;

            subvol.desc = snap.desc;
            subvol.time = snap.time;

            QString prefix = subvol.subvol.split(".snapshots").at(0).trimmed();

            if (prefix == "") {
                QString optionsOutput = runCmd("LANG=C findmnt -no options /", false, false).output.trimmed();
                if (optionsOutput.isEmpty())
                    return;

                QString subvolOption;
                for (QString option : optionsOutput.split(',')) {
                    if (option.startsWith("subvol="))
                        subvolOption = option.split("subvol=").at(1);
                }
                if (subvolOption.startsWith("/"))
                    subvolOption = subvolOption.right(subvolOption.length() - 1);

                if (subvolOption.isEmpty())
                    prefix = "root";
                else
                    prefix = subvolOption;
            } else
                prefix = prefix.left(prefix.length() - 1);

            snapperSubvolumes[prefix].append(subvol);
        }
    }

    for (QString key : snapperSubvolumes.keys())
        ui->comboBox_snapper_configs->addItem(key);
}

/*######################################################################################
 *                           Settings tab                                              *
######################################################################################*/

// Apply any changes made on the System Components and Settings pages
void GarudaAssistant::apply() {
    // cmdList holds the list of commands so they can all be executed together and the user is only for their password once
    QStringList cmdList;
    QString groupsNew, groupsRemove, packagesInstall, packagesRemove, message;
    bool serviceChanged = false;

    // Start by going through the checkboxes which have changed
    for (auto checkbox : changedCheckBoxes) {
        QString actionData = checkbox->property("actionData").toString();
        QString actionType = checkbox->property("actionType").toString();

        // Now we go through each type taking an appropriate action for each
        if (actionType == "service") {
            if (actionData != "" && unitsEnabledSet.contains(actionData) != checkbox->isChecked()) {
                setUnitState(actionData, checkbox->isChecked());
                serviceChanged = true;
            }
        } else if (actionType == "global_service") {
            if (actionData != "" && globalUnitsEnabledSet.contains(actionData) != checkbox->isChecked()) {
                setUnitState(actionData, checkbox->isChecked(), true);
                serviceChanged = false;
            }
        } else if (actionType == "group") {
            if (checkbox->isChecked() && !this->groupsEnabled.contains(actionData)) {
                if (groupsNew != "")
                    groupsNew += ",";
                groupsNew += actionData;
            } else if (!checkbox->isChecked() && this->groupsEnabled.contains(actionData)) {
                if (groupsRemove != "")
                    groupsRemove += ",";
                groupsRemove += actionData;
                runCmd("gpasswd -d " + qgetenv("USER") + " " + actionData, false, true);
            }
        } else if (actionType == "package") {
            if (checkbox->isChecked()) {
                if (packagesInstall != "")
                    packagesInstall += " ";
                packagesInstall += actionData;
            } else {
                if (packagesRemove != "")
                    packagesRemove += " ";
                packagesRemove += actionData;
            }
        } else if (actionType == "custom") {
            if (actionData == "hblock") {
                if (checkbox->isChecked() != checkHblock()) {
                    if (checkbox->isChecked())
                        cmdList.append("/usr/bin/hblock");
                    else
                        cmdList.append("/usr/bin/hblock -S none -D none");
                    message += tr("hblock settings changed, please reboot to apply") + "\n";
                }
            } else if (actionData == "gdm-wayland") {
                if (checkbox->isChecked() != checkGDMWayland()) {
                    if (checkbox->isChecked())
                        runCmd(SCRIPT_PATH "gdmwayland-on.sh", false, true);
                    else
                        runCmd(SCRIPT_PATH "gdmwayland-off.sh", false, true);
                }
            }
        }
    }

    // Empty the list of changed checkboxes since they have already been processed
    changedCheckBoxes.clear();

    // Add command for groups to be added
    if (!groupsNew.isEmpty()) {
        runCmd("usermod -aG " + groupsNew + " " + qgetenv("USER"), false, true);
        message += tr("Groups were added please logout so changes will take effect") + "\n";
    }

    // Add a message for groups to be removed
    if (!groupsRemove.isEmpty()) {
        message += tr("Groups were removed please logout so changes will take effect") + "\n";
    }

    if (serviceChanged)
        message += tr("Service state updated") + "\n";

    // Add command for packages to be installed
    if (!packagesInstall.isEmpty())
        cmdList.prepend("pacman -S " + packagesInstall + " --needed");

    // Add command for packages to be removed
    if (!packagesRemove.isEmpty())
        cmdList.prepend("pacman -Rc " + packagesRemove);

    // Check if the DNS servers need to be adjusted
    QString requestedDNS = ui->comboBox_dns->currentText();
    QString currentDNS = getCurrentDNS();

    if (requestedDNS != currentDNS && requestedDNS != "") {
        runCmd(SCRIPT_PATH "change-dns-server.sh " + dnsChoices.value(requestedDNS), false, true);
        message += tr("DNS Updated!") + "\n";
    }

    // See if the shell needs adjusting
    // We execute the shell commands seperately because we don't want them to be run with escalated privs
    QString requestedShell = ui->comboBox_shell->currentText();

    // Check if the selected shell is different from the user's default shell
    if (requestedShell != getUserDefaultShell()) {
        // check to see if we need to also copy the config
        bool copyConfig = ui->checkBox_shell->isChecked();

        if (requestedShell == "sh") {
            runCmd("usermod -s /bin/sh " + qgetenv("USER"), false, true);
        } else if (requestedShell == "bash") {
            runCmd("usermod -s /bin/bash " + qgetenv("USER"), false, true);
            if (copyConfig)
                runCmd(configActions["bash"].command, false, false);
        } else if (requestedShell == "fish") {
            if (!checkAndInstall("garuda-fish-config"))
                return;
            runCmd("usermod -s /bin/fish " + qgetenv("USER"), false, true);
            if (copyConfig)
                runCmd(configActions["fish"].command, false, false);
        } else if (requestedShell == "zsh") {
            if (!checkAndInstall("garuda-zsh-config"))
                return;
            runCmd("usermod -s /bin/zsh " + qgetenv("USER"), false, true);
            if (copyConfig)
                runCmd(configActions["zsh"].command, false, false);
        }
        message += tr("Shell changed, please relogin to apply changes!") + "\n";
    }

    // Run all the commands at once in a terminal
    if (!cmdList.empty())
        runCmdTerminal(cmdList, true);

    // Reload the list of groups & services
    loadEnabledUnits();
    loadGlobalEnabledUnits();
    loadGroups();

    refreshInterface();

    if (!message.isEmpty())
        QMessageBox::warning(0, tr("Important!!"), message);
}

// If a checkbox is clicked and that requires action, add it to a list to be processed by the apply button
void GarudaAssistant::on_checkBox_clicked(QWidget *widget) {
    QCheckBox *cb = static_cast<QCheckBox *>(widget);
    if (cb->property("actionType") != "none")
        changedCheckBoxes.insert(cb);
}

void GarudaAssistant::on_pushButton_applySettings_clicked() {
    apply();
    static_cast<QPushButton *>(QObject::sender())->clearFocus();
}

void GarudaAssistant::on_pushButton_applySystem_clicked() {
    apply();
    static_cast<QPushButton *>(QObject::sender())->clearFocus();
}

void GarudaAssistant::on_pushButton_refreshinxi_clicked() {
    ui->textEdit_inxi->clear();
    refreshInxi();
    ui->pushButton_refreshinxi->clearFocus();
}

void GarudaAssistant::on_pushButton_copyinxi_clicked() {
    QClipboard *clipboard = QGuiApplication::clipboard();

    clipboard->setText("```text\n" + ui->textEdit_inxi->toPlainText() + "\n```");
    ui->pushButton_copyinxi->clearFocus();
}

void GarudaAssistant::on_pushButton_openforum_clicked() {
    QDesktopServices::openUrl(QUrl("https://forum.garudalinux.org"));
    ui->pushButton_openforum->clearFocus();
}

void GarudaAssistant::on_pushButton_analyze_clicked() {
    ui->textEdit_diagnostics->clear();
    QString cmd = "/usr/bin/systemd-analyze --no-pager blame ; /usr/bin/systemd-analyze --no-pager critical-chain";
    diagProc.start("/usr/bin/bash", QStringList() << "-c" << cmd);

    ui->pushButton_analyze->clearFocus();
}

void GarudaAssistant::on_pushButton_journal_clicked() {
    ui->textEdit_diagnostics->clear();
    diagProc.start("/usr/bin/bash", QStringList() << "-c"
                                                  << "/usr/bin/journalctl -p err -b --no-pager");

    ui->pushButton_journal->clearFocus();
}

void GarudaAssistant::on_pushButton_copydiag_clicked() {
    QClipboard *clipboard = QGuiApplication::clipboard();

    clipboard->setText("```text\n" + ui->textEdit_diagnostics->toPlainText() + "\n```");
    ui->pushButton_copydiag->clearFocus();
}

void GarudaAssistant::on_pushButton_openforum_2_clicked() {
    QDesktopServices::openUrl(QUrl("https://forum.garudalinux.org"));
    ui->pushButton_openforum_2->clearFocus();
}
